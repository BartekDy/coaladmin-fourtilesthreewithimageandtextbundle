**Installation**  
**1.** add this to composer.json
```
    "repositories": [  
        {"type": "composer", "url": "https://pelkas.repo.repman.io"}  
    ]  
```
**2.** composer require coaladmin/four-tiles-three-with-image-and-text-bundle  
**3.** bin/console coaladmin:create-FourTilesThreeWithImageAndTextBundle 
**4.** bin/console assets:install

**Przykład**
![](src/Resources/public/ExamplePicture.png)