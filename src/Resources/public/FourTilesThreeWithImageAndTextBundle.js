import React, {useEffect, useState} from 'react';
import EditForm from "./EditForm";
import HeadingWithTextComponent
    from "../../../assets/App/components/multi_use_components/HeadingWithTextComponent";
import ImageWithHeadingAndTextComponent
    from "../../../assets/App/components/multi_use_components/ImageWithHeadingAndTextComponent";
import ComponentMenu from "../../../assets/App/components/multi_use_components/ComponentMenu";
import SectionFormBase from "../../../assets/App/components/base_components/SectionFormBase";

const FourTilesThreeWithImageAndTextBundle = ({params, id, components}) =>{
    const [firstTileFirstText, setFirstTileFirstText] = useState( '');
    const [firstTileSecondText, setFirstTileSecondText] = useState('');

    const [secondTileFirstText, setSecondTileFirstText] = useState('');
    const [secondTileSecondText, setSecondTileSecondText] = useState('');
    const [secondTileImg, setSecondTileImg] = useState('')

    const [thirdTileFirstText, setThirdTileFirstText] = useState('');
    const [thirdTileSecondText, setThirdTileSecondText] = useState('');
    const [thirdTileImg, setThirdTileImg] = useState('');

    const [fourthTileFirstText, setFourthTileFirstText] = useState('');
    const [fourthTileSecondText, setFourthTileSecondText] = useState('');
    const [fourthTileImg, setFourthTileImg] = useState('');

    const title = 'Edycja - FourTilesThreeWithImageAndTextBundle'

    const [showEditForm, setShowEditForm] = useState(false)
    useEffect(() => {
        const imgArray = params[0].elements.at(-1).img;
        params[0].elements.map((el=> {
            switch (el.name) {
                case 'Pierwszy kafelek':
                    setFirstTileFirstText(el.heading)
                    return setFirstTileSecondText(el.text)
                case 'Drugi kafelek':
                    setSecondTileFirstText(el.heading)
                    setSecondTileImg(imgArray[el.imgIndex].src)
                    return setSecondTileSecondText(el.text)
                case 'Trzeci kafelek':
                    setThirdTileFirstText(el.heading)
                    setThirdTileImg(imgArray[el.imgIndex].src)
                    return setThirdTileSecondText(el.text)
                case 'Czwarty kafelek':
                    setFourthTileFirstText(el.heading)
                    setFourthTileImg(imgArray[el.imgIndex].src)
                    return setFourthTileSecondText(el.text)
                default:
                    return null
            }
        }))
    },[])

    return(
        <>
            <section className={'FourTilesThreeWithImageAndTextBundle'} style={{display:"flex"}}>
                <div style={{display:"flex"}}>
                    <HeadingWithTextComponent  heading={firstTileFirstText}  text={firstTileSecondText}  containerClassName={'four-tiles-with-text first-Tile'} />
                </div>
                <ImageWithHeadingAndTextComponent heading={secondTileFirstText} text={secondTileSecondText} imgSrc={secondTileImg} componentClassName ={"four-tiles-with-text second-Tile"} />
                <ImageWithHeadingAndTextComponent heading={thirdTileFirstText} text={thirdTileSecondText} imgSrc={thirdTileImg} componentClassName ={"four-tiles-with-text third-Tile"} />
                <ImageWithHeadingAndTextComponent heading={fourthTileFirstText} text={fourthTileSecondText} imgSrc={fourthTileImg} componentClassName ={"four-tiles-with-text fourth-Tile"} />
                <ComponentMenu id={id} components={components} editFunction={e=>setShowEditForm(true)}/>
            </section>
            {showEditForm === true ? <SectionFormBase closeBtnFucntion={e => setShowEditForm(false)}
                                                      title={title}
                                                      formContainerClass={''}
                                                      form={<EditForm id={id}/>}
            /> : null }
        </>
    )
}

export default FourTilesThreeWithImageAndTextBundle;