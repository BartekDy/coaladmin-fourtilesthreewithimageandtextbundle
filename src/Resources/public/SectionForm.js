import React, {useRef, useState} from 'react';
import {useForm} from "react-hook-form";
import '../../FileUploader/dist/jquery.fileuploader.min'
import Fileuploader from "../../FileUploader/src/thirdparty/react/Fileuploader";

const SectionForm = ({item, currentMenuItem,sectionList,pagesList}) => {
    const [showForm, setShowForm] = useState(false)
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [fields, setFields] = useState(null);


    const {register, control, formState: {errors}} = useForm();

    const [firstTileFirstText, setFirstTileFirstText] = useState(item?.firstTileFirstText ?? '');
    const [firstTileSecondText, setFirstTileSecondText] = useState(item?.firstTileSecondText ?? '');

    const [secondTileFirstText, setSecondTileFirstText] = useState(item?.secondTileFirstText ?? '');
    const [secondTileSecondText, setSecondTileSecondText] = useState(item?.secondTileSecondText ?? '');

    const [thirdTileFirstText, setThirdTileFirstText] = useState(item?.thirdTileFirstText ?? '');
    const [thirdTileSecondText, setThirdTileSecondText] = useState(item?.thirdTileSecondText ?? '');

    const [fourthTileFirstText, setFourthTileFirstText] = useState(item?.fourthTileFirstText ?? '');
    const [fourthTileSecondText, setFourthTileSecondText] = useState(item?.fourthTileSecondText ?? '');

    const form = useRef(null);
    const [bundleName, setBundleName] = useState('FourTilesThreeWithImageAndTextBundle')



    const [send, setSend] = useState(null);
    const [pageSectionId, setPageSectionId] = useState(null)
    const resource = '/api/page_sections';

    const currentSection = sectionList.find(el => el.title === bundleName);
    const currentPage = pagesList.find((el)=>{
        var array=el.menuItem.split('/')
        if(array[array.length - 1] == currentMenuItem){
            return el.id
        }
    })

    const onSubmit = (e) => {
        e.preventDefault()
        var formData = new FormData(e.target);
        const dataToSend = {
            page: `/api/pages/${currentPage.id}`,
            section: `/api/sections/${currentSection.id}`,
            params: [
                {
                    elements: [
                        {
                            id: 1,
                            name: "Pierwszy kafelek",
                            heading: firstTileFirstText,
                            text: firstTileSecondText
                        },
                        {
                            id: 2,
                            name: "Drugi kafelek",
                            heading: secondTileFirstText,
                            text: secondTileSecondText,
                            imgIndex: 0
                        },
                        {
                            id: 3,
                            name: "Trzeci kafelek",
                            heading: thirdTileFirstText,
                            text: thirdTileSecondText,
                            imgIndex: 1
                        },
                        {
                            id: 4,
                            name: "Czwarty kafelek",
                            heading: fourthTileFirstText,
                            text: fourthTileSecondText,
                            imgIndex: 2
                        },
                        {
                            img: null
                        }
                    ]
                }
            ],
        }

        fetch(`${window.location.origin}/api/page_sections`, {
            method: 'POST',
            body: JSON.stringify(dataToSend),
            headers: {
                'Content-Type': 'application/json'
            },
        })
            .then((response) => {
                return response.json()
            })
            .then(data => {
                formData.append('pageSectionId', data.id);
                formData.append('path', 'uploads/FourTilesThreeWithImageAndText/');
                fetch(`${window.location.origin}/api/uploadImageApi`, {
                    method: 'POST',
                    body: formData,
                })
                    .then((response) => {
                        response.status === 201 ? document.getElementsByClassName('close_btn')[0].click() : null
                        window.location.reload(true)
                        return response.json()
                    })
                    .then(data => {
                    })
                    .catch((error) => {
                    });
            })
            .catch((error) => {
            });
    }




    return (
        <form method={'POST'} ref={form} className='add_section_text_with_image' onSubmit={onSubmit}>
            <div className="form-control">
                <label>Nagłówek</label>
                <input type="text" name="firstTileFirstText" {...register("firstTileFirstText")}
                       value={firstTileFirstText} onChange={(e) => setFirstTileFirstText(e.target.value)}
                />
            </div>
            <div className="form-control">
                <label>Treść</label>
                <input type="text" name="firstTileSecondText" {...register("firstTileSecondText")}
                       value={firstTileSecondText} onChange={(e) => setFirstTileSecondText(e.target.value)}
                />
            </div>
            <hr/>
            <div className="form-control">
                <label>Ikona</label>
                <Fileuploader name="files1"  limit="1" addMore={'addMore'} sortable={true} extensions="image/*" captions='pl' theme={'thumbnails'}/>
            </div>
            <div className="form-control">
                <label>Nagłówek</label>
                <input type="text" name="firstTileFirstText" {...register("secondTileFirstText")}
                       value={secondTileFirstText} onChange={(e) => setSecondTileFirstText(e.target.value)}
                />
            </div>
            <div className="form-control">
                <label>Opis</label>
                <input type="text" name="secondTileSecondText" {...register("secondTileSecondText")}
                       value={secondTileSecondText} onChange={(e) => setSecondTileSecondText(e.target.value)}
                />
            </div>
            <hr/>
            <div className="form-control">
                <label>Ikona</label>
                <Fileuploader name="files2"  limit="1" addMore={'addMore'} sortable={true} extensions="image/*" captions='pl' theme={'thumbnails'}/>
            </div>
            <div className="form-control">
                <label>Nagłówek</label>
                <input type="text" name="thirdTileFirstText" {...register("thirdTileFirstText")}
                       value={thirdTileFirstText} onChange={(e) => setThirdTileFirstText(e.target.value)}
                />
            </div>
            <div className="form-control">
                <label>Opis</label>
                <input type="text" name="thirdTileSecondText" {...register("thirdTileSecondText")}
                       value={thirdTileSecondText} onChange={(e) => setThirdTileSecondText(e.target.value)}
                />
            </div>
            <hr/>
            <div className="form-control">
                <label>Ikona</label>
                <Fileuploader name="files3"  limit="1" addMore={'addMore'} sortable={true} extensions="image/*" captions='pl' theme={'thumbnails'}/>
            </div>
            <div className="form-control">
                <label>Nagłówek</label>
                <input type="text" name="fourthTileFirstText" {...register("fourthTileFirstText")}
                       value={fourthTileFirstText} onChange={(e) => setFourthTileFirstText(e.target.value)}
                />
            </div>
            <div className="form-control">
                <label>Opis</label>
                <input type="text" name="fourthTileSecondText" {...register("fourthTileSecondText")}
                       value={fourthTileSecondText} onChange={(e) => setFourthTileSecondText(e.target.value)}
                />
            </div>
            <hr/>
            <button id="submit" type="submit" className="btn btn-block"><i
                className="far fa-save"> </i> Zapisz
            </button>
        </form>

    )
}

export default SectionForm