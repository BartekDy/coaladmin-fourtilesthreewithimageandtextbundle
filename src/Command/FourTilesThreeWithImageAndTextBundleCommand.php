<?php
namespace FourTilesThreeWithImageAndTextBundle\Command;

use App\Entity\Section;
use App\Repository\SectionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class FourTilesThreeWithImageAndTextBundleCommand extends Command
{
    private $em;
    private $sectionRepository;

    public function __construct(EntityManagerInterface $em, SectionRepository $sectionRepository)
    {
        $this->em = $em;
        $this->sectionRepository = $sectionRepository;

        parent::__construct();
    }

    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'coaladmin:create-FourTilesThreeWithImageAndTextBundle';

    protected function configure(): void
    {
        $this
            ->setDescription('Creates a new section with one tile with header and text inside and three tile witch text and image inside')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $section = $this->sectionRepository->findOneBy(['title' => 'FourTilesThreeWithImageAndTextBundle']);

        if (!$section) {
            $section = new Section();
        }

        $section
            ->setTitle('FourTilesThreeWithImageAndTextBundle')
            ->setParams($this->getParams())
            ->setTemplateUrl('@FourTilesThreeWithImageAndTextBundle/template/show.html.twig')
        ;

        $this->em->persist($section);
        $this->em->flush();

        return Command::SUCCESS;
    }

    private function getParams()
    {
        $params = [
            'elements'=> [
                [
                    "id" => 1,
                    "name" => "Pierwszy kafelek",
                    "heading" => null,
                    'text' => null
                ],
                [
                    "id" => 2,
                    "name" => "Drugi kafelek",
                    "heading" => null,
                    'text' => null,
                    'imgIndex' => 0
                ],
                [
                    "id" => 3,
                    "name" => "Trzeci kafelek",
                    "heading" => null,
                    'text' => null,
                    'imgIndex' => 1
                ],
                [
                    "id" => 4,
                    "name" => "Czwarty kafelek",
                    "heading" => null,
                    'text' => null,
                    'imgIndex' => 2
                ],
                [
                    "img" => [
                        [
                            "src" => null
                        ],
                        [
                            "src" => null
                        ],
                        [
                            "src" => null
                        ]
                    ]
                ]

            ]
        ];

        return $params;
    }
}