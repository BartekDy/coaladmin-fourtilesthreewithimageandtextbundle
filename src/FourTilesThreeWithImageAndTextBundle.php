<?php

namespace FourTilesThreeWithImageAndTextBundle;

use FourTilesThreeWithImageAndTextBundle\DependencyInjection\FourTilesThreeWithImageAndTextBundleExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class FourTilesThreeWithImageAndTextBundle extends Bundle
{
    /**
     * Overridden to allow for the custom extension alias.
     */
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new FourTilesThreeWithImageAndTextBundleExtension();
        }
        return $this->extension;
    }
}